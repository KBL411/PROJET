-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 19 déc. 2018 à 21:20
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

DROP TABLE IF EXISTS `membres`;
CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `membres`
--

INSERT INTO `membres` (`id`, `pseudo`, `pass`, `email`) VALUES
(14, 'test', '$2y$10$B5LGnc0ySbcMMqqt2DkjR./tH/4v6fb3hTx.Tp3a04j6L4fex0WA6', 'test@test.com'),
(13, 'bot', '$2y$10$ppVip5UiJrlppU73Nij01exoKMGISIE3aL7wo5UtgEB8XQDHyndQG', 'bot'),
(10, 'bot3', '$2y$10$0yl3TJAey/NU0blgZtNMG.3GgSBIdduDrqJf9GLfe3kAoOUAO03Ee', 'bot3@efrei.net'),
(11, 'bot3', '$2y$10$u658/U6uh3PYLJgD44bJ4.egD.3in7vQeKnBYBBBFU9zraXzHyAtO', 'bot3@efrei.net'),
(12, 'bot3', '$2y$10$irZjGse1.EooPKkNBHH6we/L4fvEdgAEDI.LY7xb.a5HYn9Rn7Jnq', 'bot3@efrei.net'),
(15, 'root', '$2y$10$Wk36fBKCHiZ.YmdS9B6da.ieVbryJ/P3daPlSaED74m/A28UcNwi.', '');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

DROP TABLE IF EXISTS `partie`;
CREATE TABLE IF NOT EXISTS `partie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jeu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solo_equipe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conditions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `partie`
--

INSERT INTO `partie` (`id`, `jeu`, `solo_equipe`, `conditions`) VALUES
(1, 'Call of duty WWII', 'Team n', 'll,l,l,'),
(2, 'Starcraft 2', 'Solo', 'ee'),
(3, 'Heroes of the Storm', 'Solo', 's,v'),
(4, 'Tekken 7', 'Solo', ''),
(5, 'CounterStrike: Global Offensive', 'Solo', ''),
(6, 'NBA 2K19', 'Solo', ''),
(7, 'Heroes of the Storm', 'Solo', ''),
(8, 'Starcraft 2', 'Solo', '');

-- --------------------------------------------------------

--
-- Structure de la table `tournois`
--

DROP TABLE IF EXISTS `tournois`;
CREATE TABLE IF NOT EXISTS `tournois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jeu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solo_equipe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nb_equipe` int(11) NOT NULL,
  `conditions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tournois`
--

INSERT INTO `tournois` (`id`, `jeu`, `solo_equipe`, `nb_equipe`, `conditions`) VALUES
(1, 'League of Legends', 'Solo', 10, 'tg'),
(2, 'Overwatch', 'Team n', 8, ''),
(3, 'Tekken 7', 'Team n', 8, ''),
(4, 'CounterStrike: Global Offensive', 'Team n', 8, 'toutes des putes'),
(5, 'League of Legends', 'Solo', 8, ''),
(6, 'StreetFighter V', 'Solo', 4, ''),
(7, 'StreetFighter V', 'Solo', 2, ''),
(8, 'Dota 2', 'Solo', 2, 'que des grenades'),
(9, 'Overwatch', 'Team n', 4, ''),
(10, 'Dota 2', 'Solo', 4, ''),
(11, 'Dota 2', 'Solo', 4, ''),
(12, 'Heroes of the Storm', 'Solo', 4, ''),
(13, 'Call of duty WWII', 'Solo', 32, ''),
(14, 'Call of duty WWII', 'Team n', 8, ''),
(15, 'Starcraft 2', 'Solo', 4, ''),
(16, 'Starcraft 2', 'Solo', 4, ''),
(17, 'League of Legends', 'Solo', 4, ''),
(18, 'League of Legends', 'Team n', 8, ''),
(19, 'CounterStrike: Global Offensive', 'Team n', 4, ''),
(20, 'Call of duty WWII', 'Solo', 4, '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
