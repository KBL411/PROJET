<?php
session_start();

    
if (isset($_SESSION['id']) AND isset($_SESSION['pseudo']))
{
    echo 'Vous etes connecté ' . $_SESSION['pseudo'];
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
         <title>e-sport management</title>
        <link rel="stylesheet" href="style.css" />
    </head>

    <body>
    <header class="page-header">
            <h1 class= "nomsite"> E-Sport Management</h1>
        <br/>
            <h2> Création d'un tournoi </h2>
        <nav>
            <ul>
                
                 <li>
                    <a href= accueil.php> Accueil</a>
                </li>
                <li>
                    <a href=deconnexion.php> Déconnexion</a>
                </li>
            </ul>
        </nav>
        </header>
    
    
    <form action ="traitement tournois.php" method="POST">
        

    <fieldset>
        <legend>Choissisez votre jeu</legend>

        <div>
            <input type="radio" id="League of Legends"
                   name="jeu" value="League of Legends" />
            <label for="League of Legends">League of Legends</label>
        </div>

        <div>
            <input type="radio" id="Dota 2"
                   name="jeu" value="Dota 2" />
            <label for="Dota 2">Dota 2</label>
        </div>

        <div>
            <input type="radio" id="Counter Strike: Global Offensive"
                   name="jeu" value="CounterStrike: Global Offensive" />
            <label for="CounterStrike: Global Offensive">CounterStrike: Global Offensive</label>
        </div>

        <div>
            <input type="radio" id="Heroes of the Storm"
                   name="jeu" value="Heroes of the Storm" />
            <label for="Heroes of the Storm">Heroes of the Storm</label>
        </div>

        <div>
            <input type="radio" id="Overwatch"
                   name="jeu" value="Overwatch" />
            <label for="Overwatch">Overwatch</label>
        </div>

        <div>
            <input type="radio" id="Smite"
                   name="jeu" value="Smite" />
            <label for="Smite">Smite</label>
        </div>

        <div>
            <input type="radio" id="Call of duty WWII"
                   name="jeu" value="Call of duty WWII" />
            <label for="Call of duty WWII">Call of duty WWII</label>
        </div>

        <div>
            <input type="radio" id="Starcraft 2"
                   name="jeu" value="Starcraft 2" />
            <label for="Starcraft 2">Starcraft 2</label>
        </div>

        <div>
            <input type="radio" id="Halo"
                   name="jeu" value="Halo" />
            <label for="Halo">Halo</label>
        </div>

        <div>
            <input type="radio" id="World of Tanks"
                   name="jeu" value="World of Tanks" />
            <label for="World of Tanks">World of Tanks</label>
        </div>

        <div>
            <input type="radio" id="FIFA 19"
                   name="jeu" value="FIFA 19" />
            <label for="FIFA 19">FIFA 19</label>
        </div>

        <div>
            <input type="radio" id="Super Smash Bros"
                   name="jeu" value="Super Smash Bros" />
            <label for="Super Smash Bros">Super Smash Bros</label>
        </div>

        <div>
            <input type="radio" id="Rocket League"
                   name="jeu" value="Rocket League" />
            <label for="Rocket League">Rocket League</label>
        </div>

        <div>
            <input type="radio" id="Fornite"
                   name="jeu" value="Fornite" />
            <label for="Fornite">Fornite</label>
        </div>

        <div>
            <input type="radio" id="PUBG"
                   name="jeu" value="PUBG" />
            <label for="PUBG">PUBG</label>
        </div>

        <div>
            <input type="radio" id="NBA 2K19"
                   name="jeu" value="NBA 2K19" />
            <label for="NBA 2K19">Dota 2</label>
        </div>

        <div>
            <input type="radio" id="RainbowSIX:Siege"
                   name="jeu" value="RainbowSIX:Siege" />
            <label for="RainbowSIX:Siege">RainbowSIX:Siege</label>
        </div>

        <div>
            <input type="radio" id="DragonBall Figther Z"
                   name="jeu" value="DragonBall Figther Z" />
            <label for="DragonBall Figther Z">DragonBall Figther Z</label>
        </div>

        <div>
            <input type="radio" id="Tekken 7"
                   name="jeu" value="Tekken 7" />
            <label for="Tekken 7">Tekken 7</label>
        </div>

        <div>
            <input type="radio" id="StreetFighter V"
                   name="jeu" value="StreetFighter V" />
            <label for="StreetFighter V">StreetFighter V</label>
        </div>

    </fieldset>
<br/><br/><br/><br/>
  <p>Veuillez choisir l'equipe avec laquelle vous voulez jouer :</p>
  <div>
    <input type="radio" id="Team n"
     name="equipe" value="Team n">
    <label for="Team n">Team n</label>
 <!-- On ajoutera le nombre de team que l'utilisateur peut utiliser pour  -->
    <input type="radio" id="Solo"
     name="equipe" value="Solo">
    <label for="Solo">Solo</label>
	</div>

<br/><br/>
     <label for="uname">Nombre d'equipes :</label>
         <select name="nombre_equipes">
          <option value=4>4</option>
          <option value=8>8</option>
          <option value=16>16</option>
          <option value=32>32</option>
          
     </select>
    <br/><br/><br/><br/>
	<p><label>Condition* :<input type="text" size=100 name="condition" /></label></p>
    

*(non obligatoire)

    <br/><br/>
    <input type="submit" name="Envoyer">
    </form>
    </body>
</html>