<?php 

$bdd=new PDO('mysql:host=localhost;dbname=test','root','',array (PDO:: ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
$pseudo = $_POST['pseudo'];

//  Récupération de l'utilisateur et de son pass hashé
$req = $bdd->prepare('SELECT id, pass FROM membres WHERE pseudo = :pseudo');
$req->execute(array('pseudo' => $pseudo));
$resultat = $req->fetch();

// Comparaison du pass envoyé via le formulaire avec la base
$isPasswordCorrect = password_verify($_POST['pass'], $resultat['pass']);

if (!$resultat)
{
    header('Location: connexion .php');
}
else
{
    if ($isPasswordCorrect) {
        session_start();
        $_SESSION['id'] = $resultat['id'];
        $_SESSION['pseudo'] = $pseudo;
        header('Location: accueil.php');
    }
    else {
        header('Location: connexion .php');
    }
}

?>